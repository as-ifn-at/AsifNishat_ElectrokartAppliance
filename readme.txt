Step 1: Turn on the network using minifab, before that remove all other running containers using docker system prune

Step2: Add wallets, Gateway and environment using IBM Blockchain Platform

Step3: Now deploy the chaincode after changing the version to avoid any problems

Step4: Open AutoApp and first use npm install command to install all other dependencies.

Step5: after that in terminal inside AutoAPP run command npm start.

Step6: open localhost:3000 and you will be able to interact with the project.
