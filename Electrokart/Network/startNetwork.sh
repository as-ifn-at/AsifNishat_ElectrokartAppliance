#!/bin/sh

echo "Start the network"
minifab netup -s couchdb -e true -o manufacturer.electrokart.com

sleep 5

echo "Create the channel"
minifab create -c appliancechannel
 
sleep 2

echo "Join the peers to channel"
minifab join -c appliancechannel

sleep 2

echo "Anchor update"
minifab anchorupdate

sleep 2

echo "Generate the required materials"
minifab profilegen -c appliancechannel
