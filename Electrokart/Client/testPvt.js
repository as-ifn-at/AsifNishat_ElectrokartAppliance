const { clientApplication } = require('./client');

let dealerClient = new clientApplication();
const transientData = {
    type: Buffer.from('Bulb'),
    description: Buffer.from('24W'),
    color: Buffer.from('White'),
    dealerName: Buffer.from('XXX')
}

dealerClient.generateAndSubmitTxn(
    "dealer",
    "Admin",
    "appliancechannel",
    "Electrokraft",
    "OrderContract",
    "privateTxn",
    transientData,
    "createOrder",
    "Order-02"
).then(msg => {
    console.log(msg.toString())
});