
function toManuDash() {
    window.location.href = '/manufacturer';
}

function swalBasic(data) {
    swal.fire({
        // toast: true,
        icon: `${data.icon}`,
        title: `${data.title}`,
        animation: true,
        position: 'center',
        showConfirmButton: true,
        footer: `${data.footer}`,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
            toast.addEventListener('mouseenter', swal.stopTimer)
            toast.addEventListener('mouseleave', swal.resumeTimer)
        }
    })
}

// function swalDisplay(data) {
//     swal.fire({
//         // toast: true,
//         icon: `${data.icon}`,
//         title: `${data.title}`,
//         animation: false,
//         position: 'center',
//         html: `<h3>${JSON.stringify(data.response)}</h3>`,
//         showConfirmButton: true,
//         timer: 3000,
//         timerProgressBar: true,
//         didOpen: (toast) => {
//             toast.addEventListener('mouseenter', swal.stopTimer)
//             toast.addEventListener('mouseleave', swal.resumeTimer)
//         }
//     }) 
// }

function reloadWindow() {
    window.location.reload();
}

function ManWriteData() {
    event.preventDefault();
    // const type = document.getElementById('Appliancetype').value;
    const type = document.getElementById('Appliancetype').value;
    const description = document.getElementById('Appliancedescription').value;
    // const model = document.getElementById('carModel').value;
    const color = document.getElementById('ApplianceColor').value;
    const DOM = document.getElementById('DOM').value;
    const Mfn = document.getElementById('Mfn').value;
    console.log(type + description + color + DOM + Mfn);

    if (vin.length == 0 || type.length == 0 || description.length == 0 || color.length == 0 || DOM.length == 0 || Mfn.length == 0) {
        const data = {
            title: "You might have missed something",
            footer: "Enter all mandatory fields to add a new appliance",
            icon: "warning"
        }
        swalBasic(data);
    }
    else {
        fetch('/manuwrite', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ appNumber: appNumber, type: type, description: description, color: color, DOM: DOM, Mfn: Mfn })
        })
            .then(function (response) {
                if (response.status == 200) {
                    const data = {
                        title: "Success",
                        footer: "Added a new appliance",
                        icon: "success"
                    }
                    swalBasic(data);
                } else {
                    const data = {
                        title: `Car with Vin Number ${vin} already exists`,
                        footer: "Vin Number must be unique",
                        icon: "error"
                    }
                    swalBasic(data);
                }

            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}
function ManQueryData() {

    event.preventDefault();
    const Qvin = document.getElementById('QueryVinNumbMan').value;

    console.log(Qvin);

    if (Qvin.length == 0) {
        const data = {
            title: "Enter a Valid Qvin Number",
            footer: "This is a mandatory field",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/manuread', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ QVinNumb: Qvin })
        })
            .then(function (response) {
                console.log(response);
                return response.json();
            })
            .then(function (Appdata) {
                dataBuf = Appdata["Appdata"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Appliance with Qvin ${Qvin} :`,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (error) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

//Method to get the history of an item
function getItemHistory(applianceId) {
    console.log("postalId", applianceId)
    window.location.href = '/itemhistory?applianceId=' + applianceId;
}

function getMatchingOrders(applianceId) {
    console.log("applicationId", applianceId)
    window.location.href = 'matchOrder?applianceId=' + applianceId;
}

function buyAppliance() {
    console.log("Entered the register function")
    event.preventDefault();
    const appNumber = document.getElementById('appNumber').value;
    const appOwner = document.getElementById('appOwner').value;
    const billNumber = document.getElementById('billNumber').value;
    console.log(appNumber + appOwner + billNumber);

    if (appNumber.length == 0 || appOwner.length == 0 || billNumber.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/buyAppliance', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ appNumber: appNumber, appOwner: appOwner, billNumber: billNumber })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Purchased appliance ${appNumber} to ${appOwner}`,
                        footer: "Purchased appliance",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to purchase`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function createOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('orderNumber').value;
    const appType = document.getElementById('appType').value;
    const appDesc = document.getElementById('appDesc').value;
    const appColor = document.getElementById('appColor').value;
    const dealerName = document.getElementById('dealerName').value;
    console.log(orderNumber + appColor + dealerName);

    if (orderNumber.length == 0 || appType.length == 0 || appDesc.length == 0
        || appColor.length == 0 || dealerName.length == 0) {
        const data = {
            title: "You have missed something",
            footer: "All fields are mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/createOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber, appType: appType, appDesc: appDesc, appColor: appColor, dealerName: dealerName })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order is created`,
                        footer: "Raised Order",
                        icon: "success"
                    }
                    swalBasic(data)

                } else {
                    const data = {
                        title: `Failed to create order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function readOrder() {
    console.log("Entered the order function")
    event.preventDefault();
    const orderNumber = document.getElementById('orderNumber').value;

    console.log(orderNumber);

    if (orderNumber.length == 0) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    }
    else {
        fetch('/readOrder', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderNumber: orderNumber })
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (orderData) {
                dataBuf = orderData["orderData"]
                swal.fire({
                    // toast: true,
                    icon: `success`,
                    title: `Current status of Order : `,
                    animation: false,
                    position: 'center',
                    html: `<h3>${dataBuf}</h3>`,
                    showConfirmButton: true,
                    timer: 3000,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', swal.stopTimer)
                        toast.addEventListener('mouseleave', swal.resumeTimer)
                    }
                })
            })
            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}

function matchOrder(orderId, applicationId) {
    if (!orderId || !applicationId) {
        const data = {
            title: "Enter a order number",
            footer: "Order Number is mandatory",
            icon: "warning"
        }
        swalBasic(data)
    } else {
        fetch('/match', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ orderId, applicationId })
        })
            .then(function (response) {
                if (response.status === 200) {
                    const data = {
                        title: `Order matched successfully`,
                        footer: "Order matched",
                        icon: "success"
                    }
                    swalBasic(data)
                } else {
                    const data = {
                        title: `Failed to match order`,
                        footer: "Please try again !!",
                        icon: "error"
                    }
                    swalBasic(data)
                }
            })

            .catch(function (err) {
                const data = {
                    title: "Error in processing Request",
                    footer: "Something went wrong !",
                    icon: "error"
                }
                swalBasic(data);
            })
    }
}


function allOrders() {
    window.location.href = '/allOrders';
}


function getEvent() {
    fetch('/event', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        }
    })
        .then(function (response) {
            console.log("Response is ###", response)
            return response.json()
        })
        .then(function (event) {
            dataBuf = event["applianceEvent"]
            swal.fire({
                toast: true,
                // icon: `${data.icon}`,
                title: `Event : `,
                animation: false,
                position: 'top-right',
                html: `<h5>${dataBuf}</h5>`,
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
        .catch(function (err) {
            swal.fire({
                toast: true,
                icon: `error`,
                title: `Error`,
                animation: false,
                position: 'top-right',
                showConfirmButton: true,
                timer: 3000,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener('mouseenter', swal.stopTimer)
                    toast.addEventListener('mouseleave', swal.resumeTimer)
                }
            })
        })
}